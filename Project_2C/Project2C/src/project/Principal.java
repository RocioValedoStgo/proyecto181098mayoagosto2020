package project;

import java.util.Stack;

import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.shape.Circle;
import javafx.scene.control.Button;

public class Principal extends Application {

	Parent root;

	int tam = 0;
	double clickUnoX = 0;
	double clickUnoY = 0;
	double incrementoX = 0;
	double incrementoY = 0;
	Circle circle;

	int size = 0;
	int cont = 0;
	double incrementX = 0;
	double incrementY = 0;
	Circle points;
	Circle pointsAux;

	Stack<Double> x0 = new Stack<>();
	Stack<Double> y0 = new Stack<>();

	Stack<Double> X0 = new Stack<>();
	Stack<Double> Y0 = new Stack<>();

	Stack<Double> X = new Stack<>();
	Stack<Double> Y = new Stack<>();

	@FXML
	private Slider sliderSize;

	@FXML
	private ColorPicker colorPicker;

	@FXML
	private AnchorPane scenePane;

	@FXML
	private Button btnLine;

	@FXML
	private Button btnLines;

	@FXML
	private Button btnSquare;

	@FXML
	private Button btnClear;

	@FXML
	private Button btnExit;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		root = FXMLLoader.load(getClass().getResource("root.fxml"));
		primaryStage.setTitle("Proyecto 2do Corte - Lienzo");
		primaryStage.getIcons().add(new Image("./assets/logo.png"));
		primaryStage.setScene(new Scene(root, 750, 550));
		primaryStage.show();
	}

	@FXML
	void clear(MouseEvent event) {
		scenePane.getChildren().clear();
	}

	@FXML
	void drawLine(MouseEvent event) {
		scenePane.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent click) {
				circle = new Circle();
				X0.push(click.getX());
				Y0.push(click.getY());

				if (X0.size() == 2) {
					double x1 = (double) X0.peek();
					double y1 = (double) Y0.peek();
					X0.pop();
					Y0.pop();
					double x = (double) X0.peek();
					double y = (double) Y0.peek();
					X0.pop();
					Y0.pop();

					tam = (int) Math.abs(x1 - x);
					if (Math.abs(y1 - y) > tam) {
						tam = (int) Math.abs(y1 - y);
					}
					incrementoX = (double) (x1 - x) / (double) tam;
					incrementoY = (double) (y1 - y) / (double) tam;

					x = x + 1;
					y = y + 1;

					for (int i = 1; i <= tam; ++i) {
						circle = new Circle();
						x = x + incrementoX;
						y = y + incrementoY;
						circle.setCenterX(x);
						circle.setCenterY(y);
						circle.setRadius(sliderSize.getValue());
						circle.setFill(colorPicker.getValue());
						scenePane.getChildren().addAll(circle);
					}
				}
			}
		});
		x0.clear();
		y0.clear();
	}

	@FXML
	void drawLines(MouseEvent event) {
		scenePane.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent click) {
				circle = new Circle();
				x0.push(click.getX());
				y0.push(click.getY());

				if (x0.size() == 2) {
					double x1 = (double) x0.peek();
					double y1 = (double) y0.peek();
					x0.pop();
					y0.pop();
					double x = (double) x0.peek();
					double y = (double) y0.peek();
					x0.pop();
					y0.pop();
					x0.push(x1);
					y0.push(y1);

					tam = (int) Math.abs(x1 - x);
					if (Math.abs(y1 - y) > tam) {
						tam = (int) Math.abs(y1 - y);
					}
					incrementoX = (double) (x1 - x) / (double) tam;
					incrementoY = (double) (y1 - y) / (double) tam;

					x = x + 1;
					y = y + 1;

					for (int i = 1; i <= tam; ++i) {
						circle = new Circle();
						x = x + incrementoX;
						y = y + incrementoY;
						circle.setCenterX(x);
						circle.setCenterY(y);
						circle.setRadius(sliderSize.getValue());
						circle.setFill(colorPicker.getValue());
						scenePane.getChildren().addAll(circle);
					}
				}
			}
		});
	}

	@FXML
	void drawSquare(MouseEvent event) {
		scenePane.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				System.out.println("Mouse click X : Y - " + mouseEvent.getX() + " : " + mouseEvent.getY());
				points = new Circle();

				double X0 = mouseEvent.getX();
				double Y0 = mouseEvent.getY();

				Circle circle = new Circle();
				circle.setCenterX(X0);
				circle.setCenterY(Y0);
				circle.setRadius(4);
				circle.setFill(colorPicker.getValue());

				X.push(mouseEvent.getX());
				Y.push(mouseEvent.getY());
				scenePane.getChildren().addAll(circle);

				pointsCircle();
			}
		});
		x0.clear();
		y0.clear();
	}

	public void pointsCircle() {
		if (X.size() == 2) {
			double x1 = (double) X.peek();
			double y1 = (double) Y.peek();
			X.pop();
			Y.pop();
			double x = (double) X.peek();
			double y = (double) Y.peek();
			X.pop();
			Y.pop();

			// X
			size = (int) Math.abs(x1 - x);
			if (Math.abs(y1 - y) > size) {
				size = (int) Math.abs(y1 - y);
			}
			double xAux = x;
			incrementX = (double) (x1 - x) / (double) size;
			xAux++;

			for (int i = 1; i <= size; i++) {
				points = new Circle();
				xAux = xAux + incrementX;
				points.setCenterX(xAux);
				points.setCenterY(y);
				points.setRadius(sliderSize.getValue());
				points.setFill(colorPicker.getValue());

				pointsAux = new Circle();
				pointsAux.setCenterX(xAux);
				pointsAux.setCenterY(y1);
				pointsAux.setRadius(sliderSize.getValue());
				pointsAux.setFill(colorPicker.getValue());

				scenePane.getChildren().addAll(points);
				scenePane.getChildren().addAll(pointsAux);
			}

			// Y
			size = (int) Math.abs(x1 - x);
			if (Math.abs(y1 - y) > size) {
				size = (int) Math.abs(y1 - y);
			}
			double yAux = y;
			incrementY = (double) (y1 - y) / (double) size;
			yAux++;

			for (int i = 1; i <= size; i++) {
				points = new Circle();
				yAux = yAux + incrementY;
				points.setCenterX(x);
				points.setCenterY(yAux);
				points.setRadius(sliderSize.getValue());
				points.setFill(colorPicker.getValue());

				scenePane.getChildren().addAll(points);
			}

			if (y <= y1) {
				for (int i = (int) y; i <= y1; i++) {
					points = new Circle();
					points.setCenterX(x1);
					points.setCenterY(i);
					points.setRadius(sliderSize.getValue());
					points.setFill(colorPicker.getValue());
					scenePane.getChildren().addAll(points);
				}
			} else {
				for (int i = (int) y; i >= y1; i--) {
					points = new Circle();
					points.setCenterX(x1);
					points.setCenterY(i);
					points.setRadius(sliderSize.getValue());
					points.setFill(colorPicker.getValue());
					scenePane.getChildren().addAll(points);
				}
			}
		}
	}

	@FXML
	void exit(MouseEvent event) {
		System.exit(1);
	}

	@FXML
	void size(MouseEvent event) {
		Double sizeSlide = sliderSize.getValue();

		System.out.println(sizeSlide + " Click OMG");
	}

	@FXML
	void changeColor(ActionEvent event) {
		Color color = colorPicker.getValue();

		System.out.println(color + " Change OMG");
	}

}
