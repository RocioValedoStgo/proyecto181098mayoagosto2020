package drawLines;

import java.util.Stack;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Principal extends Application {

	int size = 0;
	int cont = 0;
	double incrementX = 0;
	double incrementY = 0;
	Circle points;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Group root = new Group();
		Scene scene = new Scene(root, 720, 600, Color.rgb(238, 238, 238));
		primaryStage.setTitle("App - Lienzo");
		primaryStage.getIcons().add(new Image("/images/artista.png"));
		primaryStage.setScene(scene);
		primaryStage.show();

		Stack<Double> x0 = new Stack<>();
		Stack<Double> y0 = new Stack<>();

		scene.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				System.out.println("Mouse click X : Y - " + mouseEvent.getX() + " : " + mouseEvent.getY());
				points = new Circle();

				double X0 = mouseEvent.getX();
				double Y0 = mouseEvent.getY();

				Circle circle = new Circle();
				circle.setCenterX(X0);
				circle.setCenterY(Y0);
				circle.setRadius(4);
				circle.setFill(Color.CRIMSON);

				x0.push(mouseEvent.getX());
				y0.push(mouseEvent.getY());
				root.getChildren().addAll(circle);

				pointsCircle(x0, y0, root);

			}
		});
	}

	public void pointsCircle(Stack x0, Stack y0, Group root) {
		if (x0.size() == 2) {
			double x1 = (double) x0.peek();
			double y1 = (double) y0.peek();
			x0.pop();
			y0.pop();
			double x = (double) x0.peek();
			double y = (double) y0.peek();
			x0.pop();
			y0.pop();
			x0.push(x1);
			y0.push(y1);

			size = (int) Math.abs(x1 - x);
			if (Math.abs(y1 - y) > size) {
				size = (int) Math.abs(y1 - y);
			}
			incrementX = (double) (x1 - x) / (double) size;
			incrementY = (double) (y1 - y) / (double) size;

			x = x + 1;
			y = y + 1;

			for (int i = 1; i <= size; ++i) {
				points = new Circle();
				x = x + incrementX;
				y = y + incrementY;
				points.setCenterX(x);
				points.setCenterY(y);
				points.setRadius(2.0f);
				points.setFill(Color.CRIMSON);
				root.getChildren().addAll(points);
			}
		}
	}

}
