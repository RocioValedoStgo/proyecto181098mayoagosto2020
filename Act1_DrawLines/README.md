# DRAW LINES IN JAVAFX

# Configurations 
- Step 1
	- Create a new `User Library` under.
		> Eclipse > Project > Properties > Java Build Path > Libraries > ModulePath > Add Library > User libraries > New
	- Give it a name.
	- Select the new library.
	- Add External JavaFX 11's JAR's.
	- Search and select JavaFX 11's JAR'S.
- Step 2
	- Create a new `variable`.
		> Run > Run configurations > Java App > Main (1) > arguments > Variables > Edit Variables > New
	- Give it a name.
	- Set the value with the environment variable PATH_TO_FX.
	- `Apply and close`.
- Step 3
	- Modify Execution.
		> Run > Run configurations > Java App > Main > arguments > VM arguments
	- Set the value `--module-path ${PATH_TO_FX} --add-modules javafx.controls,javafx.fxml`.

#### Or check this
[JavaFX and Eclipse.](https://openjfx.io/openjfx-docs/)

# Run 

  #### If the configuration has been correct, press the "Run Main" button and you must start the program. :)