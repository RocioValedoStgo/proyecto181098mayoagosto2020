import java.util.Scanner;

public class Rhombus {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int value = 0;

    do {
      System.out.println("Ingrese un numero impar: ");
      value = sc.nextInt();
      if (value % 2 == 0) {
        System.out.println("El numero que usted ingreso es par");
        System.out.println("Intente de nuevo");
        System.out.println(
          "----------------------------------------------------------------------"
        );
        System.out.println("");
      }
    } while (value % 2 == 0);

    drawFullRhombus(value);
    drawRhombusOutline(value);
  }

  public static void drawFullRhombus(int value) {
    System.out.println("");

    int rows = value / 2 + 1;

    for (int i = 1; i <= rows; i++) {
      for (int j = 1; j <= rows - i; j++) {
        System.out.print(" ");
      }
      for (int k = 1; k <= (2 * i) - 1; k++) {
        System.out.print("*");
      }
      System.out.println("");
    }

    rows--;

    for (int i = 1; i <= rows; i++) {
      for (int j = 1; j <= i; j++) {
        System.out.print(" ");
      }
      for (int k = 1; k <= (rows - i) * 2 + 1; k++) {
        System.out.print("*");
      }
      System.out.println("");
    }

    System.out.println("");
  }

  public static void drawRhombusOutline(int value) {
    int rows = value / 2 + 1;

    for (int i = 0; i < rows; i++) {
      System.out.println();
      for (int j = 0; j < rows - i - 1; j++) {
        System.out.print(" ");
      }
      for (int k = 0; k < (2 * i) + 1; k++) {
        if (k == 0 || k == 2 * i || i == 0) {
          System.out.print("*");
        } else {
          System.out.print(" ");
        }
      }
    }

    for (int i = rows - 2; i >= 0; i--) {
      System.out.println();
      for (int j = 0; j < rows - i - 1; j++) {
        System.out.print(" ");
      }
      for (int k = 0; k < i * 2 + 1; k++) {
        if (k == 0 || i == 0 || k == 2 * i) {
          System.out.print("*");
        } else {
          System.out.print(" ");
        }
      }
    }
  }
}
